package core

// Or Rule
type OrRuleStruct struct {
	rules []BaseRule
}

func OrRule(rules...BaseRule) *OrRuleStruct{
	return &OrRuleStruct{rules: rules}
}

func(self *OrRuleStruct) GetRules(r...BaseRule)[]BaseRule{
	return self.rules
}

func(self *OrRuleStruct) AddRule(r...BaseRule){
	self.rules = append(self.rules, r...)
}

func(self *OrRuleStruct) Condition(ctx *Context) bool{
	for _, r := range self.rules{
		// execute the Action whenever a rule satisfied
		if r.Condition(ctx){
			r.Action(ctx)
			return true
		}
	}
	return false
}

func(self *OrRuleStruct) Action(ctx *Context){

}

func(self *OrRuleStruct) Execute(ctx *Context){
	if self.Condition(ctx){
		self.Action(ctx)
	}
}

