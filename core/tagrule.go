package core

type TagRuleStruct struct {
	tag  string
	rule BaseRule
	action func(self *TagRuleStruct, ctx *Context)
}

func TagRule(tag string, rule BaseRule, action func(self *TagRuleStruct, ctx *Context)) *TagRuleStruct {
	return &TagRuleStruct{
		tag:  tag,
		rule: rule,
		action: action,
	}
}

func(self TagRuleStruct) String() string{
	return self.tag
}

func(self *TagRuleStruct) Condition(ctx *Context) bool{
	return self.rule.Condition(ctx)
}

func(self *TagRuleStruct) Action(ctx *Context){
	self.rule.Action(ctx)
	self.action(self, ctx)
}

func(self *TagRuleStruct) Execute(ctx *Context){
	if self.rule.Condition(ctx){
		self.Action(ctx)
	}
}

func(self *TagRuleStruct) GetTag() string{
	return self.tag
}
