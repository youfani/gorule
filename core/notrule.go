package core

// Not Rule
type NotRuleStruct struct {
	rule BaseRule
}

func NotRule(r BaseRule) *NotRuleStruct{
	return &NotRuleStruct{rule: r}
}

func(self *NotRuleStruct) GetRule(r...BaseRule)BaseRule{
	return self.rule
}

func(self *NotRuleStruct) Condition(ctx *Context) bool{
	if self.rule.Condition(ctx){
		return false
	}else{
		return true
	}
}

func(self *NotRuleStruct) Action(ctx *Context){
	self.rule.Action(ctx)
}

func(self *NotRuleStruct) Execute(ctx *Context){
	if self.Condition(ctx){
		self.Action(ctx)
	}
}
