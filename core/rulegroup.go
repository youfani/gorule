package core

// RuleGroup
type RuleGroupStruct struct {
	rules []BaseRule
}

func RuleGroup(rules...BaseRule) *RuleGroupStruct {
	return &RuleGroupStruct{rules: rules}
}

func(self *RuleGroupStruct) GetRules() []BaseRule{
	return self.rules
}

func(self *RuleGroupStruct) AddRule(r...BaseRule){
	self.rules = append(self.rules, r...)
}

func(self *RuleGroupStruct) Condition(ctx *Context) bool{
	return true
}

func(self *RuleGroupStruct) Action(ctx *Context){
	for _, rule := range self.rules{
		rule.Execute(ctx)
	}
}

func(self *RuleGroupStruct) Execute(ctx *Context){
	for _, rule := range self.rules{
		rule.Execute(ctx)
	}
}