package core

type Context struct {
	data map[string]interface{}
}

func NewContext() Context {
	return Context{
		data: make(map[string]interface{}),
	}
}

func (ctx *Context) Get(k string) interface{} {
	if _, ok := ctx.data[k];ok{
		return ctx.data[k]
	}else{
		return nil
	}
}

func (ctx *Context) GetWithDefault(k string, d interface{}) interface{} {
	if _, ok := ctx.data[k];ok{
		if ctx.data[k] == nil{
			return d
		}else{
			return ctx.data[k]
		}
	}else{
		return d
	}
}

func (ctx *Context) Set(k string, v interface{}){
	ctx.data[k] = v
}

func (ctx *Context) JSON() map[string]interface{}{
	return ctx.data
}

func (ctx *Context) Update(mapObj map[string]interface{}){
	for k, v := range mapObj{
		ctx.Set(k, v)
	}
}