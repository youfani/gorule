package core

import "regexp"

// Regex Rule
type RegexRuleStruct struct {
	ReList []*regexp.Regexp
	condition func(self *RegexRuleStruct, ctx *Context) bool
	action    func(self *RegexRuleStruct, ctx *Context)
}

func(self *RegexRuleStruct) Condition(ctx *Context) bool{
	return self.condition(self, ctx)
}

func(self *RegexRuleStruct) Action(ctx *Context){
	self.action(self, ctx)
}

func(self *RegexRuleStruct) Execute(ctx *Context){
	if self.Condition(ctx){
		self.Action(ctx)
	}
}

func RegexRule(reStrList []string,
	condition func(self *RegexRuleStruct, ctx *Context) bool,
	action func(self *RegexRuleStruct, ctx *Context)) *RegexRuleStruct{
	ReList := []*regexp.Regexp{}
	for _, reStr := range reStrList{
		ReList = append(ReList, regexp.MustCompile(reStr))
	}

	regexRuleStruct := RegexRuleStruct{
		ReList:     ReList,
		condition: condition,
		action:    action,
	}
	return &regexRuleStruct
}