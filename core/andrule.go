package core

// And Rule
type AndRuleStruct struct{
	rules []BaseRule
}

func AndRule(rules...BaseRule) *AndRuleStruct{
	return &AndRuleStruct{rules: rules}
}

func(self *AndRuleStruct) GetRules(r...BaseRule)[]BaseRule{
	return self.rules
}

func(self *AndRuleStruct) AddRule(r...BaseRule){
	self.rules = append(self.rules, r...)
}

func(self *AndRuleStruct) Condition(ctx *Context) bool{
	for _, r := range self.rules{
		if !r.Condition(ctx){
			return false
		}else{
			r.Action(ctx)
		}
	}
	return true
}

func(self *AndRuleStruct) Action(ctx *Context){}

func(self *AndRuleStruct) Execute(ctx *Context){
	if self.Condition(ctx){
		self.Action(ctx)
	}
}
