package core

// Param Rule
type ParamRuleStruct struct {
	Params interface{}
	condition func(self *ParamRuleStruct, ctx *Context) bool
	action    func(self *ParamRuleStruct, ctx *Context)
}

func(self *ParamRuleStruct) Condition(ctx *Context) bool{
	return self.condition(self, ctx)
}

func(self *ParamRuleStruct) Action(ctx *Context){
	self.action(self, ctx)
}

func(self *ParamRuleStruct) Execute(ctx *Context){
	if self.Condition(ctx){
		self.Action(ctx)
	}
}

func ParamRule(params interface{},
	condition func(self *ParamRuleStruct, ctx *Context) bool,
	action func(self *ParamRuleStruct, ctx *Context)) *ParamRuleStruct{
	paramRuleStruct := ParamRuleStruct{
		Params:     params,
		condition: condition,
		action:    action,
	}
	return &paramRuleStruct
}
