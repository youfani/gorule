package main

import (
	"gorule/core"
	"log"
)

func main(){
	ctx := core.NewContext()

	// and rule
	_ = core.AndRule(
		core.NewRule(
			core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
				return true
			}),
		),
		core.NewRule(
			core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
				return true
			}),
		),
		core.NewRule(
			core.Action(func(self core.RuleInterface, ctx core.Context) error {
				log.Println("==============true")
				return nil
			}),
		),
	).Execute(ctx)
	_ = core.AndRule(
		core.NewRule(
			core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
				return true
			}),
		),
		core.NewRule(
			core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
				return false
			}),
		),
		core.NewRule(
			core.Action(func(self core.RuleInterface, ctx core.Context) error {
				log.Println("==============false")
				return nil
			}),
		),
	).Execute(ctx)

	_ = core.AndRule(
		core.NewRule(
			core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
				return false
			}),
		),
		core.NewRule(
			core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
				return true
			}),
		),
		core.NewRule(
			core.Action(func(self core.RuleInterface, ctx core.Context) error {
				log.Println("==============false")
				return nil
			}),
		),
	).Execute(ctx)

	_ = core.AndRule(
		core.NewRule(
			core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
				return false
			}),
		),
		core.NewRule(
			core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
				return false
			}),
		),
		core.NewRule(
			core.Action(func(self core.RuleInterface, ctx core.Context) error {
				log.Println("==============false")
				return nil
			}),
		),
	).Execute(ctx)

	//or rule
	_ = core.AndRule(
		core.OrRule(
			core.NewRule(
				core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
					return true
				}),
			),
			core.NewRule(
				core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
					return true
				}),
			),
		),
		core.NewRule(
			core.Action(func(self core.RuleInterface, ctx core.Context) error {
				log.Println("==============true")
				return nil
			}),
		),
	).Execute(ctx)

	_ = core.AndRule(
		core.OrRule(
			core.NewRule(
				core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
					return true
				}),
			),
			core.NewRule(
				core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
					return false
				}),
			),
		),
		core.NewRule(
			core.Action(func(self core.RuleInterface, ctx core.Context) error {
				log.Println("==============true")
				return nil
			}),
		),
	).Execute(ctx)

	_ = core.AndRule(
		core.OrRule(
			core.NewRule(
				core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
					return false
				}),
			),
			core.NewRule(
				core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
					return true
				}),
			),
		),
		core.NewRule(
			core.Action(func(self core.RuleInterface, ctx core.Context) error {
				log.Println("==============true")
				return nil
			}),
		),
	).Execute(ctx)

	_ = core.AndRule(
		core.OrRule(
			core.NewRule(
				core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
					return false
				}),
			),
			core.NewRule(
				core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
					return false
				}),
			),
		),
		core.NewRule(
			core.Action(func(self core.RuleInterface, ctx core.Context) error {
				log.Println("==============false")
				return nil
			}),
		),
	).Execute(ctx)
	
	// not rule
	_ = core.AndRule(
		core.NotRule(
			core.NewRule(
				core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
					return false
				}),
			),
		),
		core.NewRule(
			core.Action(func(self core.RuleInterface, ctx core.Context) error {
				log.Println("==============true")
				return nil
			}),
		),
	).Execute(ctx)
	_ = core.AndRule(
		core.NotRule(
			core.NewRule(
				core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
					return true
				}),
			),
		),
		core.NewRule(
			core.Action(func(self core.RuleInterface, ctx core.Context) error {
				log.Println("==============false")
				return nil
			}),
		),
	).Execute(ctx)

}
