package main

import (
	"fmt"
	"gorule/core"
	"log"
	"reflect"
	"runtime/debug"
)

var contextKeyManager = func() *core.ContextKeyMangerStruct{
	return core.ContextKeyManager()
}()
var TITLE = contextKeyManager.Register("title")

func main(){
	defer func() {
		if r := recover(); r!= nil{
			log.Fatalln(r, "\n", string(debug.Stack()))
		}
	}()

	_ = func() error{
		fmt.Println("\ntest get set")

		ctx := core.NewContext()
		fmt.Println(ctx[TITLE])
		ctx[TITLE] = "xxxx"
		fmt.Println(ctx[TITLE])
		return nil
	}()

	_ = func() error{
		fmt.Println("\ntest CopyFromContext")
		ctx := core.NewContext()
		ctx[TITLE] = "xxxx"
		ctxCopy := core.NewContext()
		fmt.Println(ctxCopy[TITLE])
		ctxCopy.CopyFromContext(ctx)
		fmt.Println(ctxCopy[TITLE])
		return nil
	}()

	_ = func() error{
		fmt.Println("\ntest GetWithDefault")
		ctx := core.NewContext()
		fmt.Println(ctx[TITLE])
		fmt.Println(ctx.GetWithDefault(TITLE, "default"), reflect.TypeOf(ctx.GetWithDefault(TITLE, "default")))
		return nil
	}()

	_ = func() error{
		fmt.Println("\ntest ToJSON")
		ctx := core.NewContext()
		fmt.Println(ctx[TITLE])
		ctx[TITLE] = "xxxx"
		fmt.Println(ctx.ToJSON())
		return nil
	}()
}