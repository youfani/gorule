package main

import (
	"log"
	"reflect"
	"runtime"
)

func a() {

}

func main()  {
	name := runtime.FuncForPC(reflect.ValueOf(a).Pointer()).Name()
	log.Println(name)
}
