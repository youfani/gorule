package main

import (
	"log"
	"reflect"
)

func main()  {

	defer func() {
		if r := recover();r!= nil{
			log.Println(r)
		}
	}()

	_ = func() error {
		a := map[string]interface{}{}
		log.Println(a["xxx"], reflect.TypeOf(a["xxx"]))
		log.Println(reflect.TypeOf(a["xxx"]).Name())
		return nil
	}()
}
