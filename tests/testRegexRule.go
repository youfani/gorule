package main

import (
	"gorule/core"
	"log"
)

var testRegexContextKeyManager = core.ContextKeyManager()
var TEXT = testRegexContextKeyManager.Register("text")

func main(){
	ctx := core.NewContext()
	ctx[TEXT] = "abc"
	_ = core.RegexRule(
		TEXT,
		[]string{`ab`},
		func(self core.RuleInterface, ctx core.Context) error {
			log.Println("匹配了")
			return nil
		},
		core.RuleLogMiddleware(),
	).Execute(ctx)
	_ = core.RegexRule(
		TEXT,
		[]string{`ac`},
		func(self core.RuleInterface, ctx core.Context) error {
			log.Println("匹配了")
			return nil
		},
		core.RuleLogMiddleware(),
	).Execute(ctx)

}