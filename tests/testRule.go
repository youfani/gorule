package main

import (
	"fmt"
	"gorule/core"
	"log"
	"runtime/debug"
)

var testRuleContextKeyManager = func() *core.ContextKeyMangerStruct{
	return core.ContextKeyManager()
}()
var BOOL = testRuleContextKeyManager.Register("bool")

func main(){
	defer func() {
		if r := recover(); r!= nil{
			log.Println(string(debug.Stack()))
		}
	}()

	ctx := core.NewContext()
	ctx[BOOL] = false

	_ = func() error{
		defer func() {
			if r := recover(); r!= nil{
				log.Println(string(debug.Stack()))
			}
		}()
		_ = core.NewRule(nil)
		return nil
	}()

	// test new rule
	_ = core.NewRule(
		core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
			return ctx[BOOL].(bool)
		}),
	).Execute(ctx)

	_ = core.NewRule(
		core.Input(func(self core.RuleInterface, ctx core.Context) core.InputType {
			value := ctx[BOOL]
			input := core.InputType{}
			input[BOOL] = !value.(bool)
			return input
		}),
		core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
			input := self.(*core.RuleStruct).Input
			return input[BOOL].(bool)
		}),
	).Execute(ctx)

	_ = core.NewRule(
		core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
			return ctx[BOOL].(bool)
		}),
		core.Action(func(self core.RuleInterface, ctx core.Context) error {
			fmt.Println("rule1")
			return nil
		}),
	).Execute(ctx)

	_ = core.NewRule(
		core.Input(func(self core.RuleInterface, ctx core.Context) core.InputType {
			value := ctx[BOOL]
			input := core.InputType{}
			input[BOOL] = !value.(bool)
			return input
		}),
		core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
			input := self.(*core.RuleStruct).Input
			return input[BOOL].(bool)
		}),
		core.Action(func(self core.RuleInterface, ctx core.Context) error {
			fmt.Println("rule1")

			return nil
		}),
	).Execute(ctx)
	fmt.Println(ctx.ToJSON())

	_ = core.NewRule(
		core.Input(func(self core.RuleInterface, ctx core.Context) core.InputType {
			value := ctx[BOOL]
			input := core.InputType{}
			input[BOOL] = !value.(bool)
			return input
		}),
		core.Condition(func(self core.RuleInterface, ctx core.Context) bool {
			input := self.(*core.RuleStruct).Input
			return input[BOOL].(bool)
		}),
		core.Action(func(self core.RuleInterface, ctx core.Context) error {
			fmt.Println("rule1")
			return nil
		}),
		core.Output(func(self core.RuleInterface, ctx core.Context) {
			log.Println("Output")
		}),
	).Execute(ctx)

	// test rule middleware
	rule := core.NewRule(
		core.Init(func(self core.RuleInterface, ctx core.Context) {
			log.Println("初始化")
			self.(*core.RuleStruct).Name = "测试中间件规则"
		}),
		core.RuleLogMiddleware(),
	)
	rule.Execute(ctx)
	rule.Execute(ctx)

}