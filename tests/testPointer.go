package main

import "fmt"

func main()  {
	a := 10
	fmt.Println(a)
	func1 := func(i int){
		i = 11
	}
	func1(a)
	fmt.Println(a)

	b := map[string]interface{}{
		"title": "xxx",
	}
	fmt.Println(b)
	func2 := func(i map[string]interface{}){
		i["title"] = "==="
	}
	func2(b)
	fmt.Println(b)


	c := 10
	fmt.Println(c)
	func3 := func(i *int){
		*i = 11
	}
	func3(&c)
	fmt.Println(c)
}
