package main

import (
	"gorule/core"
	"log"
	"runtime/debug"
)

func main() {
	defer func() {
		if r := recover(); r!= nil{
			debug.PrintStack()
		}
	}()

	_ = func() error{
		ckm := core.ContextKeyManager()
		_ = ckm.Register("name")
		_ = ckm.Register("text")
		log.Println(ckm.Keys())
		return nil
	}()

	_ = func() error{
		ckm := core.ContextKeyManager()
		_ = ckm.Register("text")
		_ = ckm.Register("text")
		return nil
	}()
}
