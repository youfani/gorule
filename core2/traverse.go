package core2

func Traverse(rule RuleInterface, f func(rule RuleInterface) error, traverseType string){
	/*
		traverseType 遍历类型 dfs-深度优先 bfs-广度优先
	*/

	if traverseType == "dfs"{
		TraverseDFS(rule, f)
	}else if traverseType == "bfs"{
		TraverseBFS(rule, f)
	}else{
		TraverseDFS(rule, f)
	}

}

func TraverseDFS(r RuleInterface, f func(rule RuleInterface) error){
	err := f(r)
	if err != nil{
		panic(err)
	}
	if _, ok := interface{}(r).(*OrRuleStruct);ok{
		rules := r.(*OrRuleStruct).GetRules()
		for _, rule := range rules{
			TraverseDFS(rule, f)
		}
	}

	if _, ok := interface{}(r).(*AndRuleStruct);ok{
		rules := r.(*AndRuleStruct).GetRules()
		for _, rule := range rules{
			TraverseDFS(rule, f)
		}
	}

	if _, ok := interface{}(r).(*NotRuleStruct);ok{
		rule := r.(*NotRuleStruct).GetRule()
		TraverseDFS(rule, f)
	}
}

func TraverseBFS(r RuleInterface, f func(rule RuleInterface) error){
	_TraverseBFS(f, []RuleInterface{r}...)
}

func _TraverseBFS(f func(rule RuleInterface) error, rules...RuleInterface){
	for _, r := range rules{
		err := f(r)
		if err != nil{
			panic(err)
		}
	}

	for _, r := range rules{
		if _, ok := interface{}(r).(*OrRuleStruct);ok{
			rules := r.(*OrRuleStruct).GetRules()
			_TraverseBFS(f, rules...)
		}else if _, ok := interface{}(r).(*AndRuleStruct);ok{
			rules := r.(*AndRuleStruct).GetRules()
			_TraverseBFS(f, rules...)
		}else if _, ok := interface{}(r).(*NotRuleStruct);ok{
			rule := r.(*NotRuleStruct).GetRule()
			_TraverseBFS(f, []RuleInterface{rule}...)
		}else{}
	}
}
