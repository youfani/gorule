package core2

import (
	"log"
	"reflect"
	"regexp"
)

// NewRule Interface
type RuleInterface interface {
	Execute(ctx Context) error
}

type InitFunc func(self RuleInterface, ctx Context)
func Init(f func(self RuleInterface, ctx Context)) InitFunc{
	return f
}

type InputType Context
// init rule input
// just exec one times
type InputFunc func(self RuleInterface, ctx Context) InputType
func Input(f func(self RuleInterface, ctx Context) InputType) InputFunc{
	return f
}

type ConditionFunc func(self RuleInterface, ctx Context) bool
func Condition(f func(self RuleInterface, ctx Context) bool) ConditionFunc{
	return f
}

type ActionFunc func(self RuleInterface, ctx Context) error
func Action(f func(self RuleInterface, ctx Context) error) ActionFunc{
	return f
}

// output to file or database
type OutputFunc func(self RuleInterface, ctx Context)
func Output(f func(self RuleInterface, ctx Context)) OutputFunc{
	return f
}

// NewRule
type RuleStruct struct {
	Name string

	Init InitFunc
	InitFlag bool

	Input InputType
	InputFunc     InputFunc

	Condition ConditionFunc
	Action    ActionFunc

	OutputFunc    OutputFunc
	Middlewares []RuleMiddleware
}

func(self *RuleStruct) String() string {
	return self.Name
}

func(self *RuleStruct) Execute(ctx Context) error{
	if !self.InitFlag{
		self.Init(self, ctx)
		self.InitFlag = true
	}
	self.Input = self.InputFunc(self, ctx)
	for _, middleware := range self.Middlewares{
		middleware.BeforeCondition(self, ctx)
	}
	cond := self.Condition(self, ctx)
	for _, middleware := range self.Middlewares{
		middleware.AfterCondition(self, ctx, cond)
	}
	if cond{
		for _, middleware := range self.Middlewares{
			middleware.BeforeAction(self, ctx)
		}
		err := self.Action(self, ctx)
		if err != nil{
			return err
		}
		for _, middleware := range self.Middlewares{
			middleware.BeforeAction(self, ctx)
		}
	}
	self.OutputFunc(self, ctx)
	return nil
}

// NewRule Constructor
func NewRule(v ...interface{}) *RuleStruct {
	init := Init(func(self RuleInterface, ctx Context) {
		self.(*RuleStruct).Name = reflect.TypeOf(self).String()
	})
	i := Input(func(self RuleInterface, ctx Context) InputType {
		return InputType{}
	})
	c := Condition(func(self RuleInterface, ctx Context) bool {
		return true
	})
	a := Action(func(self RuleInterface, ctx Context) error {
		return nil
	})
	o := Output(func(self RuleInterface, ctx Context) {
	})
	m := []RuleMiddleware{}

	for _, f := range v{
		switch x := f.(type) {
		case InitFunc:
			init = f.(InitFunc)
		case InputFunc:
			i = f.(InputFunc)
		case ConditionFunc:
			c = f.(ConditionFunc)
		case ActionFunc:
			a = f.(ActionFunc)
		case OutputFunc:
			o = f.(OutputFunc)
		case RuleMiddleware:
			m = append(m, f.(RuleMiddleware))
		default:
			log.Panic(x, "create rule params can must be InitFunc|InputFunc|ConditionFunc|ActionFunc|OutputFunc|RuleMiddleware")
		}
	}
	ruleStruct := &RuleStruct{
		Init:init,
		InputFunc:     i,
		Condition: c,
		Action:    a,
		OutputFunc:    o,
		Middlewares:m,
	}
	return ruleStruct
}

func ConditionRule(conditionFunc ConditionFunc, middlewares ...RuleMiddleware) *RuleStruct{
	rule := NewRule(conditionFunc)
	for _, middleware := range middlewares{
		rule.Middlewares = append(rule.Middlewares, middleware)
	}
	return rule
}

func Rule(conditionFunc ConditionFunc, actionFunc ActionFunc, middlewares ...RuleMiddleware) *RuleStruct{
	rule := NewRule(conditionFunc, actionFunc)
	for _, middleware := range middlewares{
		rule.Middlewares = append(rule.Middlewares, middleware)
	}
	return rule
}

// create regex rule
func RegexRule(key *contextkey, reStrList []string, actionFunc ActionFunc, middlewares ...RuleMiddleware) *RuleStruct{
	ReList := []*regexp.Regexp{}
	for _, reStr := range reStrList{
		ReList = append(ReList, regexp.MustCompile(reStr))
	}
	conditionFunc := Condition(func(self RuleInterface, ctx Context) bool {
		for _, regex := range ReList{
			if regex.MatchString(ctx[key].(string)){
				return true
			}
		}
		return false
	})
	rule := NewRule(conditionFunc, actionFunc)
	for _, middleware := range middlewares{
		rule.Middlewares = append(rule.Middlewares, middleware)
	}
	return rule
}