package core2

import (
	"log"
	"reflect"
)

// Not NewRule
type NotRuleStruct struct {
	rule RuleInterface
}

func NotRule(r RuleInterface) *NotRuleStruct{
	return &NotRuleStruct{rule: r}
}

func(self *NotRuleStruct) GetRule() RuleInterface {
	return self.rule
}

func(self *NotRuleStruct) Condition(ctx Context) bool{
	r := self.rule
	switch x := r.(type) {
	case *RuleStruct:
		if r.(*RuleStruct).Condition(self, ctx){
			return false
		}else{
			return true
		}
	case *AndRuleStruct:
		return !r.(*AndRuleStruct).Condition(ctx)
	case *OrRuleStruct:
		return !r.(*OrRuleStruct).Condition(ctx)
	case *NotRuleStruct:
		return !r.(*NotRuleStruct).Condition(ctx)
	default:
		log.Println(reflect.TypeOf(x))
		return false
	}
}

func(self *NotRuleStruct) Action(ctx Context){}

func(self *NotRuleStruct) Execute(ctx Context) error{
	if self.Condition(ctx){
		self.Action(ctx)
	}
	return nil
}
