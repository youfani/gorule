package core2

import (
	"log"
	"reflect"
)

// Or NewRule
type OrRuleStruct struct {
	rules []RuleInterface
}

func OrRule(rules...RuleInterface) *OrRuleStruct{
	return &OrRuleStruct{rules: rules}
}

func(self *OrRuleStruct) GetRules(r...RuleInterface)[]RuleInterface {
	return self.rules
}

func(self *OrRuleStruct) AddRule(r...RuleInterface){
	self.rules = append(self.rules, r...)
}

func(self *OrRuleStruct) Condition(ctx Context) bool{
	for _, r := range self.rules{
		switch x := r.(type) {
		case *RuleStruct:
			if r.(*RuleStruct).Condition(self, ctx){
				err := r.(*RuleStruct).Action(self, ctx)
				if err != nil{
					log.Panic(err)
				}
				return true
			}
		case *AndRuleStruct:
			if r.(*AndRuleStruct).Condition(ctx){
				return true
			}
		case *OrRuleStruct:
			if r.(*OrRuleStruct).Condition(ctx){
				return true
			}
		case *NotRuleStruct:
			if r.(*NotRuleStruct).Condition(ctx){
				return true
			}
		default:
			log.Println(reflect.TypeOf(x))
			return false
		}
	}
	return false
}

func(self *OrRuleStruct) Action(ctx Context){}

func(self *OrRuleStruct) Execute(ctx Context) error{
	if self.Condition(ctx){
		self.Action(ctx)
	}
	return nil
}

