package core2

import (
	"log"
	"reflect"
)

// And NewRule
type AndRuleStruct struct{
	rules []RuleInterface
}

func AndRule(rules ...RuleInterface) *AndRuleStruct{
	return &AndRuleStruct{rules: rules}
}

func(self *AndRuleStruct) GetRules(r...RuleInterface)[]RuleInterface {
	return self.rules
}

func(self *AndRuleStruct) AddRule(r...RuleInterface){
	self.rules = append(self.rules, r...)
}

func(self *AndRuleStruct) Condition(ctx Context) bool{
	for _, r := range self.rules{
		switch x := r.(type) {
		case *RuleStruct:
			if !r.(*RuleStruct).Condition(self, ctx){
				return false
			}else{
				err := r.(*RuleStruct).Action(self, ctx)
				if err != nil{
					log.Panic(err)
				}
			}
		case *AndRuleStruct:
			if !r.(*AndRuleStruct).Condition(ctx){
				return false
			}
		case *OrRuleStruct:
			if !r.(*OrRuleStruct).Condition(ctx){
				return false
			}
		case *NotRuleStruct:
			if !r.(*NotRuleStruct).Condition(ctx){
				return false
			}
		default:
			log.Println(reflect.TypeOf(x))
			return false
		}
	}
	return true
}

func(self *AndRuleStruct) Action(ctx Context){}

func(self *AndRuleStruct) Execute(ctx Context) error{
	if self.Condition(ctx){
		self.Action(ctx)
	}
	return nil
}
