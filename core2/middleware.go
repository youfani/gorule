package core2

import (
	"log"
)

type RuleMiddleware interface {
	BeforeCondition(self RuleInterface, ctx Context)
	AfterCondition(self RuleInterface, ctx Context, conditionResult bool)
	BeforeAction(self RuleInterface, ctx Context)
	AfterAction(self RuleInterface, ctx Context)
}

type RuleLogMiddlewareStruct struct {}

func(*RuleLogMiddlewareStruct) BeforeCondition(self RuleInterface, ctx Context)  {
	log.Println(self, "BeforeCondition")
}

func(*RuleLogMiddlewareStruct) AfterCondition(self RuleInterface, ctx Context, conditionResult bool)  {
	log.Println(self, "AfterCondition", conditionResult)
}

func(*RuleLogMiddlewareStruct) BeforeAction(self RuleInterface, ctx Context)  {
	log.Println(self, "BeforeAction")
}

func(*RuleLogMiddlewareStruct) AfterAction(self RuleInterface, ctx Context)  {
	log.Println(self, "AfterAction")
}

func RuleLogMiddleware() RuleMiddleware{
	return &RuleLogMiddlewareStruct{}
}