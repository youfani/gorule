package core2

import (
	"log"
)

func init() {
	log.SetFlags(log.Llongfile|log.LstdFlags)
}

// context key type
type contextkey struct {
	name string
}

// get context key name
func(self *contextkey) Name() string {
	return self.name
}

// context key manager struct
type ContextKeyMangerStruct struct {
	keys map[string]struct{}
}

// create context key manager
func ContextKeyManager() *ContextKeyMangerStruct{
	return &ContextKeyMangerStruct{keys: map[string]struct{}{}}
}

// get context key manager keys
func(self *ContextKeyMangerStruct) Keys() map[string]struct{} {
	return self.keys
}

// the only entrance of create contextkey instance
func(self *ContextKeyMangerStruct) Register(k string) *contextkey {
	if _, ok := self.keys[k]; ok{
		log.Panicf("the context key has exists %s", k)
	}
	self.keys[k] = struct{}{}
	contextkeyObj := contextkey{name:k}
	return &contextkeyObj
}

// get context key obj
func(self *ContextKeyMangerStruct) Key(k string) *contextkey {
	if _, ok := self.keys[k]; !ok{
		log.Panicf("the context key is not exists %s", k)
	}
	contextkeyObj := contextkey{name:k}
	return &contextkeyObj
}

// rule exec global context
type Context map[*contextkey]interface{}

// new context
func NewContext() Context {
	ctx := Context{}
	return ctx
}

// copy context to current context
func (ctx Context) CopyFromContext(c Context){
	for k, v := range c{
		ctx[k] = v
	}
}

// get context key value with default value
func (ctx Context) GetWithDefault(k *contextkey, d interface{}) interface{} {
	if ctx[k] == nil{
		return d
	}else{
		return ctx[k]
	}
}

// get all context data
func (ctx Context) ToJSON() map[string]interface{}{
	data := map[string]interface{}{}
	for k, v := range ctx{
		data[k.name] = v
	}
	return data
}

// set context
func(ctx Context) Set(k *contextkey, v interface{}){
	ctx[k] = v
}

// get context
func(ctx Context) Get(k *contextkey) interface{}{
	return ctx[k]
}