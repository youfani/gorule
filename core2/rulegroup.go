package core2

// rule group
// sequential execution
type RuleGroupStruct struct {
	rules []RuleInterface
}

// create rule group
func RuleGroup(rules...RuleInterface) *RuleGroupStruct {
	return &RuleGroupStruct{rules: rules}
}

// get rule group all rules
func(self *RuleGroupStruct) GetRules() []RuleInterface {
	return self.rules
}

// add rule to rule group
func(self *RuleGroupStruct) AddRule(r...RuleInterface){
	self.rules = append(self.rules, r...)
}

// execution rule group
func(self *RuleGroupStruct) Execute(ctx Context) error{
	for _, rule := range self.rules{
		err := rule.Execute(ctx)
		if err != nil{
			return err
		}
	}
	return nil
}