#规则引擎（Go语言实现）

## 背景
规则引擎主要是用于实现规则的定义与执行的框架。  
规则本质上就是满足给定的条件之后执行相应的动作。
即 ```规则 := 条件 -> 动作```，其中条件和动作都可以用一个函数表示。  
为了能够将规则引擎的规则定义与具体的规则实例化分离，需要使用能够支持匿名函数传参的编程语言。  
通过调研发现，支持匿名函数传参的高级语言有：Python、JavaScript、Go。  
但是Python的匿名函数只支持lambda，lambda只能定义简单的一行函数，所以不适合；  
而JavaScript主要是用于网页端的脚本开发，显然也不合适；  
Go语言对于匿名函数有很好的支持，而且Go语言的性能接近C语言，所以最终我选择了使用Go语言来实现规则引擎。

## 规则引擎设计
- 上下文管理器
- 简单规则与复合规则
- 正则表达式规则
- 序列解析规则

## 使用示例

### 简单规则
```
var SimpleRule = core.Rule(
    func(ctx *core.Context) bool {
        return true
    },
    func(ctx *core.Context) {
        fmt.Println("这是一个简单规则")
    },
)
```

### 复合规则
```
// 文书类型
var CompositeRule = core.OrRule(
	core.Rule(
        func(ctx *core.Context) bool {
            return true
        },
        func(ctx *core.Context) {
            fmt.Println("子规则1")
        },
    ),
	core.Rule(
        func(ctx *core.Context) bool {
            return true
        },
        func(ctx *core.Context) {
            fmt.Println("子规则2")
        },
    ),
)
```

### 正则表达式规则
```
var RegexRuleExample = core.RegexRule(
	[]string{
		fmt.Sprintf(
			`(?m)xxx`,
		),
	},
	func(self *core.RegexRuleStruct, ctx *core.Context) bool {
		text := ctx.Get("xxx").(string)
		return self.ReList[0].MatchString(text)
	},
	func(self *core.RegexRuleStruct, ctx *core.Context) {
		fmt.Println("匹配了正则表达式")
	},
)
```

### 序列解析规则
```
// 将文本按行打标签
core.SeqTextTagParser(ctx).AddRule(
    &rule1,
    &rule2,
).Parse(ctx)
```